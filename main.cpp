//+++++++++++
//Shunting Yard - A program which utilizes the shunting yard algorithm in order to convert
//an infix expression into a binary expression tree and output it in any notation
//Andrew Hett
//5-2-2021
//------

#include <cstring>
#include <iostream>
#include "queue.h"
#include "stack.h"
#include "binary_node.h"
#include <map>
#include <math.h>

using namespace std;

map<char,pair<int,int>> operator_lookup = {
	{'^',{4,1}},
	{'*',{3,0}},
	{'/',{3,0}},
	{'+',{2,0}},
	{'-',{2,0}}
};

bool is_valid_notation(char* notation_string) {
	bool valid = false;
	if (!strcmp(notation_string,"INFIX")) {
		valid = true;
	} else if (!strcmp(notation_string,"PREFIX")) {
		valid = true;
	} else if (!strcmp(notation_string,"POSTFIX")) {
		valid = true;
	}
	return valid;
}

bool is_operator(int token) {
	bool is_op = false;
	switch ((char)token) {
	case '+':
	case '-':
	case '*':
	case '/':
	case '^':
		is_op = true;
		break;
	}
	return is_op;
}

//recursive traversal method for infix notation
void traverse_infix(BinaryNode<int*>* head) {
	if (head != nullptr) {
		if (is_operator(*head->getValue())) {
			cout << '(';
		}
		traverse_infix(head->getLeft());
		if (is_operator(*head->getValue())) {
			cout << (char)*head->getValue();
		} else {
			cout << *head->getValue();
		}
		cout << ' ';
		traverse_infix(head->getRight());
		if (is_operator(*head->getValue())) {
			cout << ')';
		}
	}
	return;
}

//recursive traversal method for prefix notation
void traverse_prefix(BinaryNode<int*>* head) {
	if (head != nullptr) {
		if (is_operator(*head->getValue())) {
			cout << (char)*head->getValue();
		} else {
			cout << *head->getValue();
		}
		cout << ' ';
		traverse_prefix(head->getLeft());
		traverse_prefix(head->getRight());
	}
	return;
}

//recursive traversal method for postfix notation
void traverse_postfix(BinaryNode<int*>* head) {
	if (head != nullptr) {
		traverse_postfix(head->getLeft());
		traverse_postfix(head->getRight());
		if (is_operator(*head->getValue())) {
			cout << (char)*head->getValue();
		} else {
			cout << *head->getValue();
		}
		cout << ' ';
	}
}

int main() {
	//allocate the stack and queue for the shunting
	//yard algorithm
	Stack<int*>* operators = new Stack<int*>();
	Queue<int*>* output = new Queue<int*>();
	cout << "Enter an infix expression: ";
	char input_buffer[50];
	char output_notation[50];
	char infix[50];
	//get the infix expression and the preferred output notation
	cin.getline(input_buffer,50);
	while (!is_valid_notation(output_notation)) {
		cout << "Format to convert to: (INFIX, PREFIX, POSTFIX): ";
		cin >> output_notation;
		for (int i=0; i<(int)strlen(output_notation); i++) {
			output_notation[i] = toupper(output_notation[i]);
		}
	}
	//strip the spaces from the infix expression
	int o=0;
	for (int i=0; i<(int)strlen(input_buffer); i++) {
		if (input_buffer[i] != ' ') {
			infix[o] = input_buffer[i];
			o++;
		}
	}
	//read the infix expression character by character
	for (int i=0; i<o; i++) {
		//if the character is a numeric digit, compute
		//the numeric value of the n-digit number starting
		//at the current index and enqueue that value
		if (isdigit(infix[i])) {
			char token[10];
			int j=0;
			int* value = new int(0);
			while (isdigit(infix[i])) {
				token[j] = infix[i];
				j++;
				i++;
			}
			i--;
			for (int l=0; l<j; l++) {
				*value += pow(10,j-l-1) * (int)(token[l]-'0');
			}
			output->enqueue(value);
		} else if (infix[i] == '(') {
			operators->push(new int(infix[i]));
		} else if (infix[i] == ')') {
			//pop all of the operators until the matching
			//left parenthesis is reached
			while (*operators->peek() != '(') {
				output->enqueue(operators->pop());
			}
			operators->pop();
		} else {
			//enqueue the operator under specific precendece conditions,
			//otherwise push it to the operator stack
			int precedence = operator_lookup[infix[i]].first;
			while (operators->peek() != nullptr) {
				if (*operators->peek() != '('
				        && (operator_lookup[*operators->peek()].first > precedence
				            || (operator_lookup[*operators->peek()].first == precedence
				                && operator_lookup[*operators->peek()].second == 0))) {
					output->enqueue(operators->pop());
				} else {
					break;
				}
			}
			operators->push(new int(infix[i]));
		}
	}
	//enqueue all of the operators left in
	//the operator stack
	while (operators->peek() != nullptr) {
		output->enqueue(operators->pop());
	}
	//construct the binary expression tree
	Stack<BinaryNode<int*>*>* tree_stack = new Stack<BinaryNode<int*>*>();
	while (output->peek() != nullptr) {
		if (!is_operator(*output->peek())) {
			tree_stack->push(new BinaryNode<int*>(output->dequeue()));
		} else {
			BinaryNode<int*>* operator_node = new BinaryNode<int*>(output->dequeue());
			operator_node->setRight(tree_stack->pop());
			operator_node->setLeft(tree_stack->pop());
			tree_stack->push(operator_node);
		}
	}
	BinaryNode<int*>* head = tree_stack->pop();
	//traverse the binary expression tree in order to
	//output the expression in the desired notation
	switch(output_notation[0]) {
	case 'I':
		//output in infix
		traverse_infix(head);
		cout << endl;
		break;
	case 'P':
		switch (output_notation[1]) {
		case 'R':
			//output in prefix
			traverse_prefix(head);
			cout << endl;
			break;
		case 'O':
			//output in postfix
			traverse_postfix(head);
			cout << endl;
			break;
		}
		break;
	}
	delete operators;
	delete output;
	delete tree_stack;
	delete head;
	return 0;
}
