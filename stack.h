#include "node.h"
#include "binary_node.h"

#ifndef STACK_H
#define STACK_H

template <typename T>
class Stack {
private:
	Node<T>* _head;
public:
	//push a token to the top of the stack
	void push(T value) {
		Node<T>* new_node = new Node<T>(value);
		if (_head == nullptr) {
			_head = new_node;
		} else {
			new_node->setNext(_head);
			_head = new_node;
		}
	}
	//pop a token from the top of the stack
	T pop() {
		Node<T>* out = _head;
		_head = _head->getNext();
		return out->getValue();
	}
	//peek at the top of the stack
	T peek() {
		if (_head != nullptr) {
			return _head->getValue();
		} else {
			return nullptr;
		}
	}
	//constructor
	Stack() {
		_head = nullptr;
	}
	//destructor
	~Stack() {
		delete _head;
	}
};

#endif
