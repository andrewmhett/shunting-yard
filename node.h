#ifndef NODE_H
#define NODE_H

template <typename T>
class Node {
private:
	Node<T>* _next;
	T _value;
public:
	//getter/setter for next node
	void setNext(Node<T>* next) {
		this->_next = next;
	}
	Node<T>* getNext() {
		return _next;
	}
	//getter/setter for stored value
	void setValue(T value) {
		this->_value = value;
	}
	T getValue() {
		return _value;
	}
	//constructor
	Node(T value) {
		this->_next = nullptr;
		this->_value = value;
	}
	//destructor
	~Node() {
		delete _value;
		delete _next;
	}
};

#endif
