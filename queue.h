#include "node.h"

#ifndef QUEUE_H
#define QUEUE_H

template <typename T>
class Queue {
private:
	Node<T>* _head;
public:
	//enqueue a node
	void enqueue(T value) {
		Node<T>* new_node = new Node<T>(value);
		if (_head == nullptr) {
			_head = new_node;
		} else {
			Node<T>* tmp_head = _head;
			while (tmp_head->getNext() != nullptr) {
				tmp_head = tmp_head->getNext();
			}
			tmp_head->setNext(new_node);
		}
	}
	//dequeue a node
	T dequeue() {
		Node<T>* out = _head;
		_head = _head->getNext();
		return out->getValue();
	}
	//peek at the front of the queue
	T peek() {
		if (_head != nullptr) {
			return _head->getValue();
		} else {
			return nullptr;
		}
	}
	//constructor
	Queue() {
		_head = nullptr;
	}
	//destructor
	~Queue() {
		delete _head;
	}
};

#endif