#ifndef BNODE_H
#define BNODE_H

template <typename T>
class BinaryNode {
private:
	BinaryNode<T>* _left;
	BinaryNode<T>* _right;
	T _value;
public:
	//getter/setter for left child
	void setLeft(BinaryNode<T>* left) {
		this->_left = left;
	}
	BinaryNode<T>* getLeft() {
		return _left;
	}
	//getter/setter for right child
	void setRight(BinaryNode<T>* right) {
		this->_right = right;
	}
	BinaryNode<T>* getRight() {
		return _right;
	}
	//getter/setter for token value
	void setValue(T token) {
		this->_value = token;
	}
	T getValue() {
		return _value;
	}
	//constructor
	BinaryNode(T value) {
		this->_left = nullptr;
		this->_right = nullptr;
		this->_value = value;
	}
	//destructor
	~BinaryNode() {
		delete _value;
		delete _left;
		delete _right;
	}
};

#endif
